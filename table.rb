line_width = 60
page = ['Table of Contents', 'Chapter 1:  Getting Started', 'Chapter 2:  Numbers', 'Chapter 3:  Letters', 'page  1', 'page  9', 'page 13']
puts page[0].center(line_width)
puts page[1].ljust(line_width/2) + page[4].rjust(line_width/2)
puts page[2].ljust(line_width/2) + page[5].rjust(line_width/2)
puts page[3].ljust(line_width/2) + page[6].rjust(line_width/2)
