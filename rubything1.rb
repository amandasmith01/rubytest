word = 'blank'
words = []
puts 'You can type as many words as you want, one word per line.'
puts 'When you are done, hit \'Enter\' on an empty line.'
while word != ''
word = gets.chomp
words = words.push word
end
puts ''
puts 'Your original words:'
puts ''
puts words
puts ''
puts 'Your words in alphabetical order:'
puts words.sort
puts ''
