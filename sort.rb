#program with recursion method

def sort some_array  #this "wraps" recursive_sort
  recursive_sort some_array, []
end

def recursive_sort unsorted_array, sorted_array

x = 0
word = 'word1'
unsorted_array = []
sorted_array = []

puts 'You can type as many words as you want, one word per line.'
puts 'When you are done, hit \'Enter\' on an empty line.'

while word != ''
word = gets.chomp
unsorted_array [x] = unsorted_array.push word
# now the words that the user entered are in the unsorted array


if x + 1 <= word
  sorted_array = sorted_array.push x + 1
  unsorted_array = unsorted_array.pop x + 1
  x = x + 1
end

if unsorted_array == 0
  puts 'Here are your words in alphabetical order:'
end

end

end

puts sort []

# OK. So we want to sort an array of words, and we know how to find out
# which of two words comes first in the dictionary (using <).
# What strikes me as probably the easiest way to do this is to keep two
# more lists around: one will be our list of already-sorted words, and the
# other will be our list of still-unsorted words. We’ll take our list of words,
# find the “smallest” word (that is, the word that would come first in the
# dictionary), and stick it at the end of the already-sorted list. All of the
# other words go into the still-unsorted list. Then you do the same thing
# again but using the still-unsorted list instead of your original list: find
# the smallest word, move it to the sorted list, and move the rest to the
# unsorted list. Keep going until your still-unsorted list is empty.
